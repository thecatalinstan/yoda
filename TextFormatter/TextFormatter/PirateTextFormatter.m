//
//  PirateTextFormatter.m
//  TextFormatter
//
//  Created by Cătălin Stan on 22/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "PirateTextFormatter.h"

@implementation PirateTextFormatter


- (id)initWithString:(NSString *)aString delegate:(id<TextFormatterDelegate>)aDelegate {
    self = [super initWithString:aString delegate:aDelegate];
    if ( self != nil ) {
        search = [NSArray arrayWithObjects:@"e", @"l", nil];
        replace = [NSArray arrayWithObjects:@"3", @"|", nil];
    }
    return self;
}

- (NSString *)performEncoding {
    [super performEncoding];
    
    NSString* response = self.string;
    for ( int idx = 0; idx < search.count; idx++ ) {
        response = [response stringByReplacingOccurrencesOfString:search[idx] withString:replace[idx]];
    }
    
    return response;
}


@end
