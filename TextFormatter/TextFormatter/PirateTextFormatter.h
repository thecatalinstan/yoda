//
//  PirateTextFormatter.h
//  TextFormatter
//
//  Created by Cătălin Stan on 22/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "TextFormatter.h"

@interface PirateTextFormatter : TextFormatter {
    NSArray* search;
    NSArray* replace;
}


@end
