//
//  TextFormatterDelegate.h
//  TextFormatter
//
//  Created by Cătălin Stan on 19/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//
#import <Foundation/Foundation.h>


@class TextFormatter;

@protocol TextFormatterDelegate <NSObject>

@optional
-(void)textFormatter:(TextFormatter*)formatter willSendString:(NSString*)aString;
-(void)textFormatter:(TextFormatter*)formatter didSendString:(NSString*)aString;
-(void)textFormatter:(TextFormatter*)formatter didReceiveFormattedString:(NSString*)response;
-(void)textFormatter:(TextFormatter*)formatter didFailWithError:(NSError**)error;

@end
