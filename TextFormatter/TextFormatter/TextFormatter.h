//
//  TextFormatter.h
//  TextFormatter
//
//  Created by Cătălin Stan on 17/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextFormatterDelegate.h"

@interface TextFormatter : NSObject {
    
    id<TextFormatterDelegate> _delegate;
    NSString* _string;
    
}

@property (nonatomic, assign) id<TextFormatterDelegate> delegate;
@property (nonatomic, retain) NSString* string;

#pragma mark Initializers
- (id)initWithString:(NSString*)aString;
- (id)initWithString:(NSString*)aString delegate:(id<TextFormatterDelegate>)aDelegate;

#pragma mark Formatting
- (void)encodeString;
- (void)encodeString:(NSString*)aString;
- (NSString*)performEncoding;

#pragma mark Delegation
- (void)willSendString:(NSString*)aString;
- (void)didSendString:(NSString*)aString;
- (void)didReceiveFormattedString:(NSString*)response;
- (void)didFailWithError:(NSError**)error;


@end
