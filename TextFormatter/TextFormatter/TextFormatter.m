//
//  TextFormatter.m
//  TextFormatter
//
//  Created by Cătălin Stan on 17/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "TextFormatter.h"

@implementation TextFormatter

@synthesize string = _string;

//+ (void)load {
//    [super load];
//    NSLog(@"%s", __PRETTY_FUNCTION__);
//}

#pragma mark Delegate
- (id<TextFormatterDelegate>)delegate {
    @synchronized(_delegate) {
        return _delegate;
    }
}

- (void)setDelegate:(id<TextFormatterDelegate>)delegate {

    @synchronized(_delegate) {
        _delegate = nil;
        if ( delegate != nil ) {
            _delegate = delegate;
        }
    }
}

#pragma mark Initializers
- (id)initWithString:(NSString *)aString {
    return [self initWithString:aString delegate:nil];
}

- (id)initWithString:(NSString *)aString delegate:(id<TextFormatterDelegate>)aDelegate {
    self = [super init];
    if ( self != nil ) {
        self.string = aString;
        self.delegate = aDelegate;
    }
    return self;
}

#pragma mark Formatting
- (void)encodeString {
    [self encodeString:_string];
}

- (void)encodeString:(NSString*)aString {
    
    if ( aString == nil ) {
        NSError* error = [NSError errorWithDomain:[[NSBundle mainBundle] bundleIdentifier] code:-1 userInfo:[NSDictionary dictionaryWithObject:NSLocalizedString(@"No string provided",nil) forKey:NSLocalizedDescriptionKey] ];
        [self didFailWithError:&error];
        return;
    }
    
    [self setString:aString];
    
    [self willSendString:self.string];

    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* response = [self performEncoding];
        [self didReceiveFormattedString:response];
    });
}

- (NSString*)performEncoding {
    [self didSendString:_string];
    return self.string;
}

#pragma mark Delegation
- (void)willSendString:(NSString*)aString {
    if ( self.delegate != nil && [self.delegate respondsToSelector:@selector(textFormatter:willSendString:)] ) {
        [self.delegate textFormatter:self willSendString:aString];
    }
}

- (void)didSendString:(NSString*)aString {
    if ( self.delegate != nil && [self.delegate respondsToSelector:@selector(textFormatter:didSendString:)] ) {
        [self.delegate textFormatter:self didSendString:aString];
    }
}

- (void)didReceiveFormattedString:(NSString*)response {
    if ( self.delegate != nil && [self.delegate respondsToSelector:@selector(textFormatter:didReceiveFormattedString:)] ) {
        [self.delegate textFormatter:self didReceiveFormattedString:response];
    }
}

- (void)didFailWithError:(NSError**)error {
    if ( self.delegate != nil && [self.delegate respondsToSelector:@selector(textFormatter:didFailWithError:)] ) {
        [self.delegate textFormatter:self didFailWithError:error];
    }
}


@end
