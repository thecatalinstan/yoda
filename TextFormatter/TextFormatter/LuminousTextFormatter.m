//
//  LuminousTextFormatter.m
//  TextFormatter
//
//  Created by Cătălin Stan on 19/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "LuminousTextFormatter.h"

@implementation LuminousTextFormatter

#pragma mark Formatting

- (NSString *)performEncoding {
    [super performEncoding];

    NSDictionary* headers = [NSDictionary dictionaryWithObject:@"8BJjusYC9WiYAYuZalKrvVudwxu8YJeM" forKey: @"X-Mashape-Authorization"];
    NSDictionary* parameters = [NSDictionary dictionary];
    
    
    UNIHTTPStringResponse* response = [[UNIRest get:^(UNISimpleRequest* request) {
        [request setUrl:[NSString stringWithFormat:@"https://yoda.p.mashape.com/yoda?sentence=%@", [self.string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        [request setHeaders:headers];
        [request setParameters:parameters];
    }] asString];

    return response.body;
}

@end
