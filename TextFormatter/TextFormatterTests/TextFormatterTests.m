//
//  TextFormatterTests.m
//  TextFormatterTests
//
//  Created by Cătălin Stan on 17/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TextFormatter.h"
#import "LuminousTextFormatter.h"

@interface TextFormatterTests : XCTestCase<TextFormatterDelegate>

@end

@implementation TextFormatterTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLuminousTextFormatter {
    LuminousTextFormatter* yoda = [[LuminousTextFormatter alloc] initWithString:@"This is a very interesting proposition" delegate:self];
    [yoda encodeString];
}

#pragma mark TextFormatterDelegate
-(void)textFormatter:(TextFormatter*)formatter willSendString:(NSString*)aString {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, aString);
}

-(void)textFormatter:(TextFormatter*)formatter didSendString:(NSString*)aString {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, aString);
}

-(void)textFormatter:(TextFormatter*)formatter didReceiveFormattedString:(NSString*)response {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, response);
}

-(void)textFormatter:(TextFormatter*)formatter didFailWithError:(NSError**)error {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, *error);
}



@end
