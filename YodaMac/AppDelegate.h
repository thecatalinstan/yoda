//
//  AppDelegate.h
//  YodaMac
//
//  Created by Cătălin Stan on 18/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
