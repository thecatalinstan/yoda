//
//  main.m
//  Yoda
//
//  Created by Cătălin Stan on 17/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
