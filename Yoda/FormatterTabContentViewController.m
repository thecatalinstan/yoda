//
//  FormatterTabContentViewController.m
//  Yoda
//
//  Created by Cătălin Stan on 21/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "FormatterTabContentViewController.h"
#import "TextFormatterViewController.h"
#import <TextFormatter/TextFormatterFramework.h>
#import "AppDelegate.h"

@interface FormatterTabContentViewController ()

@end

@implementation FormatterTabContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.textFormatterViewController = [[TextFormatterViewController alloc] initWithNibName:@"TextFormatterViewController" title:self.tabBarItem.title];
    
    [self.textFormatterViewController.view setFrame:self.view.frame];
    [self.view addSubview:self.textFormatterViewController.view];
    
    // Get the class name of the formatter we're supposed to use for this tab
    NSString* className = @"TextFormatter";
    NSInteger textFormatterTabIndex = self.tabBarItem.tag - 1;
    AppDelegate* dlg = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ( textFormatterTabIndex >= 0 && textFormatterTabIndex < dlg.textFormatters.count ) {
        className = [dlg.textFormatters objectAtIndex:textFormatterTabIndex];
    }
    
    // Initialize the formatter
    Class formatterClass = NSClassFromString(className);
    self.textFormatterViewController.textFormatter = [[formatterClass alloc] initWithString:nil delegate:self.textFormatterViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
