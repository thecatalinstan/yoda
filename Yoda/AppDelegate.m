//
//  AppDelegate.m
//  Yoda
//
//  Created by Cătălin Stan on 17/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.textFormatters = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"TextFormatters"];

    // Ensure that the text formatter classes will be loaded into the runtime
    [[NSBundle bundleForClass:[LuminousTextFormatter class]] load];
    [[NSBundle bundleForClass:[PirateTextFormatter class]] load];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{

}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{

}

- (void)applicationWillTerminate:(UIApplication *)application
{

}


@end
