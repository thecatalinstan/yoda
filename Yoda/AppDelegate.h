//
//  AppDelegate.h
//  Yoda
//
//  Created by Cătălin Stan on 17/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TextFormatter/TextFormatterFramework.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSArray* textFormatters;

@end
