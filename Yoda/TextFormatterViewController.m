//
//  TextFormatterViewController.m
//  Yoda
//
//  Created by Cătălin Stan on 21/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import "TextFormatterViewController.h"
#import "FormatterTabContentViewController.h"

@interface TextFormatterViewController ()

@end

@implementation TextFormatterViewController

@synthesize textFormatter = _textFormatter;
@synthesize historyItems = _historyItems;

- (NSArray *)historyItemsAtIndexes:(NSIndexSet *)indexes {
    return [self.historyItems objectsAtIndexes:indexes];
}

- (void)addHistoryItemsObject:(NSString*)object {
    [self willChangeValueForKey:@"historyItems"];
    [self.historyItems addObject:object];
    [self didChangeValueForKey:@"historyItems"];
}

- (void)removeHistoryItemsAtIndexes:(NSIndexSet *)indexes {
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [self willChangeValueForKey:@"historyItems"];
        [self.historyItems removeObjectAtIndex:idx];
        [self didChangeValueForKey:@"historyItems"];
    }];
}

- (NSUInteger)countOfHistoryItems {
    return self.historyItems.count;
}

- (id)initWithNibName:(NSString *)nibNameOrNil title:(NSString*)aTitle
{
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        [self setTitle:aTitle];
        [self setTextFormatter:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Load history for this particular tab
    _historyItemsKey = [self.title stringByAppendingString:@" History"];
    NSArray* items = [[NSUserDefaults standardUserDefaults] arrayForKey:_historyItemsKey];
    [self setHistoryItems:[NSMutableArray arrayWithArray:items]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self addObserver:self forKeyPath:@"historyItems" options:NSKeyValueObservingOptionNew context:nil];
    
    [self.tableView registerClass: [UITableViewCell class] forCellReuseIdentifier:@"Cell"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
//    NSLog(@"%@: %@", keyPath, change);
    
    [[NSUserDefaults standardUserDefaults] setObject:self.historyItems forKey:_historyItemsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)sendText:(id)sender {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    [self.textFormatter encodeString:self.inputText.text];
    [self.inputText setText:@""];
}

#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.inputText resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self sendText:nil];
}

#pragma mark TextFormatterDelegate
-(void)textFormatter:(TextFormatter*)formatter willSendString:(NSString*)aString {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, aString);
}

-(void)textFormatter:(TextFormatter*)formatter didSendString:(NSString*)aString {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, aString);
}

-(void)textFormatter:(TextFormatter*)formatter didReceiveFormattedString:(NSString*)response {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, response);

    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:self.title message:response delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    [self addHistoryItemsObject:response];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.historyItems.count - 1 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)textFormatter:(TextFormatter*)formatter didFailWithError:(NSError**)error {
    NSLog(@"%s, %@", __PRETTY_FUNCTION__, *error);
}

#pragma mark Keyboard Notifications
- (void)keyboardWillShow:(NSNotification *)notification {
    [self moveView:YES info:notification.userInfo];
}

- (void)keyboardDidShow:(NSNotification *)notification {
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    [self moveView:NO info:notification.userInfo];
}

- (void)keyboardDidHide:(NSNotification *)notification {
    
}

#pragma mark Keyboard Animation
- (void)moveView:(BOOL)up info:(NSDictionary*)userInfo {
    NSNumber* duration = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration.doubleValue];
    [UIView setAnimationCurve:curve];
    
    CGRect rect = self.view.frame;
    if (up) {
        rect.origin.y -= keyboardFrame.size.height - 50;
    } else {
        rect.origin.y += keyboardFrame.size.height - 50;
    }
    [self.view setFrame:rect];
    [UIView commitAnimations];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.historyItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *item = self.historyItems[indexPath.row];
    cell.textLabel.text = [item description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self removeHistoryItemsAtIndexes:[NSIndexSet indexSetWithIndex:indexPath.row]];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.inputText setText:[self.historyItems objectAtIndex:indexPath.row]];
    [self.inputText endEditing:YES];
}

#pragma mark UITableViewDelegate



@end
