//
//  FormatterTabContentViewController.h
//  Yoda
//
//  Created by Cătălin Stan on 21/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TextFormatterViewController;

@interface FormatterTabContentViewController : UIViewController

@property (strong, nonatomic) TextFormatterViewController* textFormatterViewController;

@end
