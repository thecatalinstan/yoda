//
//  TextFormatterViewController.h
//  Yoda
//
//  Created by Cătălin Stan on 21/11/13.
//  Copyright (c) 2013 Cătălin Stan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TextFormatter/TextFormatterFramework.h>

@interface TextFormatterViewController : UIViewController<TextFormatterDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    TextFormatter* _textFormatter;
    NSMutableArray* _historyItems;
    NSString* _historyItemsKey;
}

@property (strong, nonatomic) TextFormatter* textFormatter;
@property (strong, nonatomic) NSMutableArray* historyItems;

@property (weak, nonatomic) IBOutlet UITextField *inputText;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)sendText:(id)sender;

- (id)initWithNibName:(NSString *)nibNameOrNil title:(NSString*)aTitle;

-(void)keyboardWillShow:(NSNotification*)notification;
-(void)keyboardDidShow:(NSNotification*)notification;
-(void)keyboardWillHide:(NSNotification*)notification;
-(void)keyboardDidHide:(NSNotification*)notification;

@end
